FROM #{FROM}

#{RESIN_XBUILD_START}

RUN pip install pipenv

RUN mkdir -p /app
WORKDIR /app
COPY Pipfile Pipfile.lock ./
COPY submodules submodules

RUN pipenv install --two --system --deploy

COPY . .

ENTRYPOINT ["/ros_entrypoint.sh", "python", "serverapp.py"]
CMD []

#{RESIN_XBUILD_END}

import rospy
from std_msgs.msg import UInt8MultiArray


class SubscriberFactory:
    def __init__(self, iface_id, sender, numtopics):
        self.iface_id = iface_id
        self.sender = sender
        self.subscribers = []
        self.numtopics = numtopics

    def getSendCallback(self, frame_id):
        def callback(packet):
            data = bytes(packet.data)
            # rospy.loginfo(rospy.get_caller_id + "%d", frame_id)
            if data is not None:
                self.sender(frame_id, data)
        return callback

    def subscribeToAll(self):
        for frame_id in range(0, self.numtopics):
            callback = self.getSendCallback(frame_id)
            address = self.iface_id + "/" + str(frame_id) + "/tx"
            subscriber = rospy.Subscriber(address, UInt8MultiArray, callback)
            self.subscribers.append(subscriber)

        return self.subscribers


class PublisherFactory:
    def __init__(self, iface_id, numtopics):
        self.iface_id = iface_id
        self.publishers = []
        self.numtopics = numtopics

    def createAllPublishers(self, queue_size=10):
        for frame_id in range(0, self.numtopics):
            address = self.iface_id + "/" + str(frame_id) + "/rx"
            publisher = rospy.Publisher(
                address, UInt8MultiArray, queue_size=queue_size)
            self.publishers.append(publisher)

        return self.publishers

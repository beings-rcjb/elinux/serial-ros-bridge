import sys

# Definitions


def _arrToBytes_py3(array):
    return bytes(array)


def _arrToBytes_py2(array):
    return ''.join([chr(ch) for ch in array])


def _bitToBytes_py3(bit):
    return bytes([bit])


def _bitToBytes_py2(bit):
    return bit


def _bitRaw_py2(bit):
    return ord(bit)


def _bitRaw_py3(bit):
    return bit


if sys.version_info >= (3, 0):
    # Python 3
    arrToBytes = _arrToBytes_py3
    bitToBytes = _bitToBytes_py3
    bitRaw = _bitRaw_py3
else:
    # Python 2
    arrToBytes = _arrToBytes_py2
    bitToBytes = _bitToBytes_py2
    bitRaw = _bitRaw_py2

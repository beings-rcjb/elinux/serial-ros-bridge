#!/bin/sh

devices='amd64 armv7hf armv7hf-xbuild'

for device in $devices; do
	case "$device" in
		'amd64')
			baseImage='quay.io/ethanwu10/ros-pip:kinetic-ros-base'
			;;
		'armv7hf')
			baseImage='quay.io/ethanwu10/armv7hf-ros:kinetic-ros-base'
			;;
		'armv7hf-xbuild')
			baseImage='quay.io/ethanwu10/armv7hf-ros:kinetic-ros-base'
			resinXbuild=1
			;;
	esac
	sed -e "s@#{FROM}@${baseImage}@g" \
		-e "s@#{RESIN_XBUILD_START}@${resinXbuild+RUN [ \"cross-build-start\" ]}@g" \
		-e "s@#{RESIN_XBUILD_END}@${resinXbuild+RUN [ \"cross-build-end\" ]}@g" \
		Dockerfile.tpl > Dockerfile.${device}
done

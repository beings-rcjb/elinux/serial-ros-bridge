from __future__ import print_function

from serial import serial_for_url
from hdlc import HDLC_Protocol
import rospy
import bytesutil
from SubPubFactory import SubscriberFactory, PublisherFactory
import concurrent.futures as futures
import sys

g_protocol = None


# TODO: Text error handlers


def send_data(frame_id, data):
    if frame_id < 0 or frame_id > 255:
        sys.stderr.write(
            "[ERROR] send_data: Invalid frame ID '{}'\n".format(frame_id))
    if g_protocol is None:
        sys.stderr.write("[ERROR] send_data: Protocol not yet initialized\n")
    g_protocol.write(bytesutil.arrToBytes([frame_id]) + bytes(data))


class ReaderDispatcher(HDLC_Protocol):

    def __init__(self, executor, pub_fac, *args, **kwargs):
        super(ReaderDispatcher, self).__init__(*args, **kwargs)
        self.senderExecutor = executor
        self.publishers = pub_fac.createAllPublishers()

    def dispatch(self, publisher, data):
        try:
            publisher.publish(data=list(data))
        except Exception as e:
            print(
                "[ERROR] ReaderDispatcher.dispatch: "
                "Exception thrown in publish:\n{}\n\n"
                .format(e))

    def handle_frame(self, frame):
        frame_id = frame[0]
        try:
            publisher = self.publishers[frame_id]
            self.senderExecutor.submit(self.dispatch, publisher, frame[1:])
        except KeyError:
            pass


if __name__ == '__main__':
    import os
    from argparse import ArgumentParser
    from serialtrt import ReaderThread
    parser = ArgumentParser(description="")  # TODO: Description
    parser.add_argument('-d', '--serial-device', type=str,
                        required=True,
                        help="Serial device to use")
    parser.add_argument('-b', '--baud', type=int,
                        default=115200,
                        help="Port baud rate")
    parser.add_argument('-t', '--timeout', type=float,
                        default=0.1,
                        help="Protocol frame timeout in seconds")
    parser.add_argument('-n', '--name', type=str,
                        default=os.environ.get('NODE_NAME', 'serial'),
                        help="ROS node name")
    parser.add_argument('-nt', '--numtopics', type=int,
                        default=16, help="Number of distinct subscribers and number of distinct publishers")
    parser.add_argument('-A', '--anonymous', action='store_true',
                        help="Initialize ROS node in anonymous mode")

    args = parser.parse_args()

    rospy.init_node(args.name, anonymous=args.anonymous)
    with serial_for_url(args.serial_device) as ser:
        ser.baudrate = args.baud
        ser.timeout = args.timeout
        with futures.ThreadPoolExecutor(max_workers=2) as executor:
            with ReaderThread(ser, lambda: ReaderDispatcher(executor, PublisherFactory("serial0", args.numtopics)))\
                    as protocol:
                g_protocol = protocol
                sub_fac = SubscriberFactory("serial0", send_data, args.numtopics)
                sub_fac.subscribeToAll()

                rospy.spin()

    g_protocol = None
